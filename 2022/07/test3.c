#include "helpers/helpers.h"
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define SIZE 7


int strlen_no_lf(const char* str)
{
    int count = strlen(str);
    if (str[count - 1] == '\n')
	return count - 1;
    return count;
}

int main(void)
{
    struct Vector vec = vector_create();
    vector_push(&vec, 23);
    vector_push(&vec, 5);
    vector_push(&vec, 12);

    vector_print(&vec);
    vector_sort(&vec);
    vector_print(&vec);

    return 0;
}
