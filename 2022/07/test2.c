#include "helpers/helpers.h"
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define SIZE 24

void split(struct Buffer* src, struct BufferPool *pool);

int main()
{
    FILE* fd = fopen("./example.txt", "r");
    char buf[SIZE];
    fgets(buf, SIZE, fd);
    fgets(buf, SIZE, fd);
    fgets(buf, SIZE, fd);
    fgets(buf, SIZE, fd);

    struct Buffer buffer = buffer_create();
    struct BufferPool pool = buffer_pool_create();

    buffer_write_string(&buffer, buf, strlen(buf));

    /* printf("%s\n", buffer.data); */

    split(&buffer, &pool);

    for (int i = 0; i < pool.count; i++) {
        printf("%s\n", pool.buffers[i].data);
    }

    for (int i = 0; i < pool.count; i++) {
        free((pool.buffers + i)->data);
    }
    return 0;
}

void split(struct Buffer* src, struct BufferPool *pool)
{
    int length = src->count, prev = 0;
    struct Buffer local = buffer_create();
    for (int i = 0; i < length; i++) {
        if (!isspace(src->data[i])) {
            buffer_write_char(&local, src->data[i]);
            prev=1;
        } else if (prev) {
            buffer_pool_add(pool, &local);
            local = buffer_create();
            prev=0;
        }
    }
}
