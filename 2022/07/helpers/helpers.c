#include "helpers.h"
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct BufferPool buffer_pool_create()
{
    struct Buffer* buf = calloc(BUFFER_MAX, sizeof(struct Buffer));
    return (struct BufferPool){.count = 0, .max = BUFFER_MAX, .buffers = buf};
}

void buffer_pool_resize(struct BufferPool* pool)
{
    struct Buffer* ptr = realloc(pool->buffers, (BUFFER_MAX + pool->max) *
                                                    sizeof(struct Buffer));
    pool->max += BUFFER_MAX;
    pool->buffers = ptr;
}

void buffer_pool_add(struct BufferPool* pool, struct Buffer* buffer)
{
    if (pool->count == pool->max)
        buffer_pool_resize(pool);
    pool->buffers[pool->count] = *buffer;
    pool->count++;
}

void buffer_pool_print(struct BufferPool* pool)
{
    for (int i = 0; i < pool->count; i++) {
        printf("%d: %s\n", i, (pool->buffers + i)->data);
    }
}

void buffer_print(struct Buffer* buffer)
{
    int buf_count = buffer->count - 1;
    puts("[");
    for (int i = 0; i < buf_count; i++) {
        printf("%c, ", buffer->data[i]);
    }
    printf("%c", buffer->data[buf_count]);
    puts("]\n");
}

struct Buffer buffer_create()
{
    char* buf = calloc(BUFFER_MAX, sizeof(char));
    return (struct Buffer){.count = 0, .max = BUFFER_MAX, .data = buf};
}

void buffer_resize(struct Buffer* buffer)
{
    char* ptr =
        realloc(buffer->data, (BUFFER_MAX + buffer->max) * sizeof(char));
    buffer->max += BUFFER_MAX;
    buffer->data = ptr;
}

void buffer_write_string(struct Buffer* buffer, const char* str, int length)
{
    if ((buffer->count + length + 1) == buffer->max)
        buffer_resize(buffer);
    strcpy(buffer->data, str);
    buffer->count += length + 1;
}

void buffer_write_char(struct Buffer* buffer, const char c)
{
    if ((buffer->count + 1) == buffer->max)
        buffer_resize(buffer);
    buffer->data[buffer->count] = c;
    buffer->count++;
    buffer->data[buffer->count] = 0x00;
}

void split(struct Buffer* src, struct BufferPool* pool)
{
    int length = src->count, prev = 0;
    struct Buffer local = buffer_create();
    char c;
    for (int i = 0; i < length; i++) {
        c = src->data[i];
        if ((!isspace(c) && c != '\0')) {
            buffer_write_char(&local, c);
            prev = 1;
        }
        else if (prev) {
            buffer_pool_add(pool, &local);
            local = buffer_create();
            prev = 0;
        }
    }
}

void dirs_resize(struct Control* control)
{
    struct Directory* ptr = realloc(control->d, (control->max + DIRS_SIZE) *
                                                    sizeof(struct Directory));
    control->d = ptr;
    control->max += DIRS_SIZE;
}

void dirs_push(struct Control* control, struct File_t* f, int total)
{
    if (control->count == control->max)
        dirs_resize(control);
    control->d[control->count] =
        (struct Directory){.file_t = f, .total = total};
    control->count++;
}

int get_dir_size(struct File_t* file, struct Control* control)
{
    if (file->type == REG) {
        return -1;
    }
    int count = file->data.d->count, size = 0;
    for (int i = 0; i < count; i++) {
        if (file->data.d->files[i].type == REG) {
            size += file->data.d->files[i].data.size;
        }
        else
            size += get_dir_size(&file->data.d->files[i], control);
    }
    dirs_push(control, file, size);
    return size;
}

struct File_t* create_add_dir(struct File_t* holder, const char* name)
{
    struct File_t* d = new_directory(name, DIR, holder);
    add_file_t(holder, d);
    return d;
}

void create_add_file_t(struct File_t* holder, const char* name, int size)
{
    struct File_t f = create_regular(name, size);
    add_file_t(holder, &f);
}

void resize_file_t(struct File_t* file_t)
{
    struct File_t* ptr = realloc(file_t->data.d->files,
                                 (file_t->data.d->max + MAX) * sizeof(File_t));
    if (!ptr) {
        fprintf(stderr, "out of memory");
        exit(1);
    }
    file_t->data.d->files = ptr;
    file_t->data.d->max += MAX;
}

void add_file_t(struct File_t* dir, const struct File_t* file)
{
    if (dir->data.d->count == dir->data.d->max)
        resize_file_t(dir);
    int count = dir->data.d->count;
    dir->data.d->files[count] = *file;
    dir->data.d->count++;
}

struct File_t create_regular(const char* name, int size)
{
    struct File_t f;
    strcpy_safe(f.name, name, STR_LEN);
    f.type = REG;
    f.data.size = size;
    return f;
}

struct File_t* new_directory(const char* name, enum Type type,
                             struct File_t* parent)
{
    File_t* file_o = malloc(sizeof(File_t) * MAX);
    dir* dir_o = malloc(sizeof(dir));
    dir_o->files = file_o;
    dir_o->parent = parent;
    dir_o->max = MAX;
    dir_o->count = 0;

    File_t* new_dir = malloc(sizeof(File_t));
    strcpy_safe(new_dir->name, name, STR_LEN);
    new_dir->type = type;
    new_dir->data.d = dir_o;

    return new_dir;
}

struct File_t create_file_t(const char* name, enum Type type, struct dir* data)
{
    struct File_t f;
    strcpy_safe(f.name, name, STR_LEN);
    f.type = type;
    f.data.d = data;
    return f;
}

struct dir create_dir(struct File_t* parent)
{
    struct dir d = {NULL, parent, 0, MAX};
    return d;
}

struct File_t* is_file_t_exists(struct File_t* holder, enum Type type,
                                const char* name)
{
    for (int i = 0; i < holder->data.d->count; i++) {
        if (holder->data.d->files[i].type == type) {
            /* printf("%s\n", holder->data.d->files[i].name); */
            if (!strcmp(holder->data.d->files[i].name, name))
                return &holder->data.d->files[i];
        }
    }
    return NULL;
}

void strcpy_safe(char* copy_to, const char* copy_from, int max)
{
    for (int i = 0; i < max - 1; i++) {
        copy_to[i] = copy_from[i];
    }
    copy_to[max - 1] = '\0';
}

static void vector_resize(struct Vector* vec)
{
    int* ptr = realloc(vec->data, (vec->max + VEC_MAX) * sizeof(int));
    vec->data = ptr;
    vec->max += VEC_MAX;
}

struct Vector vector_create()
{
    int* ptr = malloc(sizeof(int) * VEC_MAX);
    return (struct Vector){.data = ptr, .count = 0, .max = VEC_MAX};
}

void vector_push(struct Vector* vec, int num)
{
    if (vec->count == vec->max) {
        vector_resize(vec);
    }
    vec->data[vec->count++] = num;
    /* vec->count++; */
}

int vector_peek_at(struct Vector* vec, int at)
{
    if (vec->count == at) {
        return -1;
    }
    return vec->data[at];
}

int vector_peek_from_back(struct Vector* vec, int at)
{
    if ((vec->count + at) < 0) {
        return vec->data[0];
    }
    return vec->data[vec->count + at];
}

void vector_sort(struct Vector* vec)
{
    int temp, count = vec->count;
    for (int i = 0; i < count; ++i) {
        for (int j = i + 1; j < count; ++j) {
            if (vec->data[i] >= vec->data[j]) {
                temp = vec->data[i];
                vec->data[i] = vec->data[j];
                vec->data[j] = temp;
            }
        }
    }
}

void vector_sort2(struct Vector* vec)
{
    int temp, count = vec->count;
    for (int i = 0; i < count; ++i) {
        for (int j = 0; j < count - 1 - i; ++j) {
            if (vec->data[j] > vec->data[j + 1]) {
                temp = vec->data[i];
                vec->data[i] = vec->data[j];
                vec->data[j] = temp;
            }
        }
    }
}

void vector_print(struct Vector* vec)
{
    int count = vec->count - 1;
    putc('[', stdout);
    for (int i = 0; i < count; i++) {
        printf("%d, ", vec->data[i]);
    }
    printf("%d]\n", vec->data[count]);
}
