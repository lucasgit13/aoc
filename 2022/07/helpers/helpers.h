#ifndef HELPERS_H
#define HELPERS_H

#define BUFFER_MAX 24
#define MAX 12
#define STR_LEN 24
#define DIRS_SIZE 10

typedef struct File_t File_t;
typedef struct file file;
typedef struct dir dir;

enum Type { ROOT, REG, DIR };

struct Directory {
    File_t* file_t;
    int total;
};

struct Control {
    struct Directory* d;
    int max;
    int count;
};

struct dir {
    File_t* files;
    File_t* parent;
    int count;
    int max;
};

struct File_t {
    enum Type type;
    char name[STR_LEN];
    union data {
        int size;
        dir* d;
    } data;
};

struct Buffer {
    char* data;
    int count;
    int max;
};

struct BufferPool {
    struct Buffer* buffers;
    int count;
    int max;
};

struct BufferPool buffer_pool_create();
void buffer_pool_resize(struct BufferPool* pool);
void buffer_pool_add(struct BufferPool* pool, struct Buffer* buffer);
void buffer_pool_print(struct BufferPool* pool);

struct Buffer buffer_create();
void buffer_resize(struct Buffer* buffer);
void buffer_write_string(struct Buffer* buffer, const char* str, int size);
void buffer_write_char(struct Buffer* buffer, const char c);
void buffer_print(struct Buffer* buffer);

void split(struct Buffer* src, struct BufferPool* pool);

struct dir create_dir(struct File_t* parent);
struct File_t create_file_t(const char* name, enum Type type, struct dir* data);
struct File_t create_regular(const char* name, int size);
struct File_t* new_directory(const char* name, enum Type type,
                             struct File_t* parent);
struct File_t* is_file_t_exists(struct File_t* holder, enum Type type, const char* name);
void add_file_t(struct File_t* dir, const struct File_t* file);
int get_dir_size(struct File_t* file, struct Control* control);
void create_add_file_t(struct File_t* holder, const char* name, int size);
void resize_file_t(struct File_t* file_t);
void strcpy_safe(char* copy_to, const char* copy_from, int max);
void dirs_push(struct Control* control, struct File_t* f, int total);
void dirs_resize(struct Control* control);
struct File_t* create_add_dir(struct File_t* holder, const char* name);

/* Vector */

#define VEC_MAX 32

struct Vector {
    int* data;
    int count;
    int max;
};

struct Vector vector_create();
void vector_print(struct Vector* vec);
void vector_push(struct Vector* vec, int num);
int vector_peek_at(struct Vector* vec, int at);
int vector_peek_from_back(struct Vector* vec, int at);
void vector_sort(struct Vector* vec);
void vector_sort2(struct Vector* vec);

#endif
