#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX 12
#define STR_LEN 12
#define DIRS_SIZE 10
#define TARGET 100000

typedef struct File_t File_t;
typedef struct file file;
typedef struct dir dir;

enum Type { ROOT, REG, DIR };

struct Directory {
    File_t* file_t;
    int total;
};

struct Control {
    struct Directory* d;
    int max;
    int count;
};

struct dir {
    File_t* files;
    File_t* parent;
    int count;
    int max;
};

struct File_t {
    enum Type type;
    char name[12];
    union data {
        int size;
        dir* d;
    } data;
};

struct dir create_dir(struct File_t* parent);
struct File_t create_file_t(const char* name, enum Type type, struct dir* data);
struct File_t create_regular(const char* name, int size);
struct File_t* new_directory(const char* name, enum Type type,
                             struct File_t* parent);
void add_file_t(struct File_t* dir, const struct File_t* file);
int get_dir_size(struct File_t* file, struct Control* control);
void create_add_file_t(struct File_t* holder, const char* name, int size);
void resize_file_t(struct File_t* file_t);
void strcpy_safe(char* copy_to, const char* copy_from, int max);
void dirs_push(struct Control* control, struct File_t* f, int total);
void dirs_resize(struct Control* control);
struct File_t* create_add_dir(struct File_t* holder, const char* name);

int main(void)
{
    File_t* root = new_directory("/", ROOT, NULL);
    create_add_file_t(root, "b.txt", 14848514);
    create_add_file_t(root, "c.dat", 8504156);

    File_t* a = create_add_dir(root, "a");
    create_add_file_t(a, "f", 29116);
    create_add_file_t(a, "g", 2557);
    create_add_file_t(a, "h.lst", 62596);
    
    File_t* e = create_add_dir(a, "e");
    create_add_file_t(e, "i", 584);

    File_t* d = create_add_dir(root, "d");
    create_add_file_t(d, "j", 4060174);
    create_add_file_t(d, "d.log", 8033020);
    create_add_file_t(d, "d.ext", 5626152);
    create_add_file_t(d, "k", 7214296);

    int count = 0;
    for (int i = 0; i < root->data.d->count; i++) {
        /* printf("type: %d\tname: %s\n", root->data.d->files[i].type
         * ,root->data.d->files[i].name); */
        if (root->data.d->files[i].type == REG) {
            count += root->data.d->files[i].data.size;
        }
    }

    struct Directory* dirs = malloc(sizeof(struct Directory) * DIRS_SIZE);
    struct Control control = {dirs, DIRS_SIZE, 0};

    count = get_dir_size(root, &control);
    /* printf("total size = %d\n", count); */

    for (int i=0; i<control.count; i++) {
        count = control.d[i].total;
        if (count<=TARGET) {
            printf("%s: %d\n", control.d[i].file_t->name, control.d[i].total);
        }
    }

    return 0;
}

void dirs_resize(struct Control* control)
{
    struct Directory* ptr = realloc(control->d, (sizeof(struct Directory) * control->max) + DIRS_SIZE);
    control->d = ptr;
    control->max+=DIRS_SIZE;
}

void dirs_push(struct Control* control, struct File_t* f, int total)
{
    if(control->count==control->max) dirs_resize(control);
    control->d[control->count] = (struct Directory){.file_t = f, .total = total};
    control->count++;
}

int get_dir_size(struct File_t* file, struct Control* control)
{
    if (file->type == REG) {
        return -1;
    }
    int count = file->data.d->count, size = 0;
    for (int i = 0; i < count; i++) {
        if (file->data.d->files[i].type == REG) {
            size += file->data.d->files[i].data.size;
        }
        else
            size += get_dir_size(&file->data.d->files[i], control);
    }
    dirs_push(control, file, size);
    return size;
}

struct File_t* create_add_dir(struct File_t* holder, const char* name)
{
    struct File_t* d = new_directory(name, DIR, holder);
    add_file_t(holder, d);
    return d;
}

void create_add_file_t(struct File_t* holder, const char* name, int size)
{
    struct File_t f = create_regular(name, size);
    add_file_t(holder, &f);
}

void resize_file_t(struct File_t* file_t)
{
    struct File_t* ptr = realloc(file_t->data.d->files,
                                 (file_t->data.d->max + MAX) * sizeof(File_t));
    if (!ptr) {
        fprintf(stderr, "out of memory");
        exit(1);
    }
    file_t->data.d->files = ptr;
    file_t->data.d->max += MAX;
}

void add_file_t(struct File_t* dir, const struct File_t* file)
{
    if (dir->data.d->count == dir->data.d->max)
        resize_file_t(dir);
    int count = dir->data.d->count;
    dir->data.d->files[count] = *file;
    dir->data.d->count++;
}

struct File_t create_regular(const char* name, int size)
{
    struct File_t f;
    strcpy_safe(f.name, name, STR_LEN);
    f.type = REG;
    f.data.size = size;
    return f;
}

struct File_t* new_directory(const char* name, enum Type type,
                             struct File_t* parent)
{
    File_t* file_o = malloc(sizeof(File_t) * MAX);
    dir* dir_o = malloc(sizeof(dir));
    dir_o->files = file_o;
    dir_o->parent = parent;
    dir_o->max = MAX;
    dir_o->count = 0;

    File_t* new_dir = malloc(sizeof(File_t));
    strcpy_safe(new_dir->name, name, STR_LEN);
    new_dir->type = type;
    new_dir->data.d = dir_o;

    return new_dir;
}

struct File_t create_file_t(const char* name, enum Type type, struct dir* data)
{
    struct File_t f;
    strcpy_safe(f.name, name, STR_LEN);
    f.type = type;
    f.data.d = data;
    return f;
}

struct dir create_dir(struct File_t* parent)
{
    struct dir d = {NULL, parent, 0, MAX};
    return d;
}

void strcpy_safe(char* copy_to, const char* copy_from, int max)
{
    for (int i = 0; i < max - 1; i++) {
        copy_to[i] = copy_from[i];
    }
    copy_to[max - 1] = '\0';
}
