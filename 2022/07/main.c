#include "helpers/helpers.h"
#include <assert.h>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define CHAR_BUF_SIZE 32

#define TARGET 100000
#define UPDATE_SIZE 30000000
#define FILESYSTEM_MAX 70000000

#define DIRNAME (pool.buffers + 1)->data
#define FILENAME (pool.buffers + 1)->data
#define FILESIZE pool.buffers->data
#define COMMAND (pool.buffers + 1)->data
#define CD_ARG (pool.buffers + 2)->data

enum Input { INPUT_CMD, INPUT_DIR, INPUT_FILE };
int get_type(const char c);

int main(void)
{

    FILE* fd = fopen("./input.txt", "r");
    char buf[CHAR_BUF_SIZE];

    struct Buffer buffer = buffer_create();
    struct BufferPool pool = buffer_pool_create();

    struct File_t* root = new_directory("/", ROOT, NULL);
    struct File_t* cwd = root;

    /* fgets(buf, CHAR_BUF_SIZE, fd); */
    int length = 0, file_size = 0, counter = 0;
    while (fgets(buf, CHAR_BUF_SIZE, fd)) {
        length = strlen(buf) + 1;
        buffer_write_string(&buffer, buf, length);
        split(&buffer, &pool);

        /* printf("INPUT = %d - %s\n", get_type(pool.buffers->data), buf); */
        switch (get_type(pool.buffers->data[0])) {
        case INPUT_CMD: {
            if (!strcmp(COMMAND, "ls"))
                break;
            if (!strcmp(CD_ARG, "..")) {
                if (!cwd->data.d->parent) {
                    fprintf(stderr, "no parent directory before %s\n",
                            cwd->name);
                    exit(1);
                }
                cwd = cwd->data.d->parent;
                /* printf("cd to: %s\n", cwd->name); */
                break;
            }
            if (!strcmp(CD_ARG, "/")) {
                cwd = root;
                break;
            }
            if (!(cwd = is_file_t_exists(cwd, DIR, CD_ARG))) {
                cwd = create_add_dir(cwd, CD_ARG);
                break;
            }
            /* printf("cd - %s\n", buf); */
        } break;
        case INPUT_DIR: {
            if (!is_file_t_exists(cwd, DIR, DIRNAME)) {
                create_add_dir(cwd, DIRNAME);
                /* printf("dir - %s\n", buf); */
            }
        } break;
        case INPUT_FILE: {
            /* printf("INPUT_FILE: %s\n", buf); */
            if (is_file_t_exists(cwd, REG, FILENAME))
                break;
            file_size = atoi(FILESIZE);
            create_add_file_t(cwd, FILENAME, file_size);
            /* printf("file - %s\n", buf); */
        } break;
        }

        pool.count = 0;
        buffer.count = 0;
        counter++;
    }

    /* printf("count = %d\n", root->data.d->count); */

    struct Directory* dirs = malloc(sizeof(struct Directory) * DIRS_SIZE);
    struct Control control = {dirs, DIRS_SIZE, 0};
    struct Vector vec = vector_create(), to_deleted = vector_create();

    int unused_space = 0, space_needed = 0;

    int root_size = get_dir_size(root, &control), sum = 0, count = 0;

    for (int i = 0; i < control.count; i++) {
        count = control.d[i].total;
        if (count <= TARGET) {
            sum += count;
        }
        vector_push(&vec, count);
        /* printf("%s: %d\n", control.d[i].file_t->name, control.d[i].total); */
    }

    unused_space = FILESYSTEM_MAX - root_size;
    space_needed = UPDATE_SIZE - unused_space;
    /* vector_sort(&vec); */

    for (int i = 0; i < vec.count; i++) {
        count = vec.data[i];
        if (count >= space_needed) {
            vector_push(&to_deleted, count);
        }
    }

    vector_sort(&to_deleted);

    printf("Part 1 answer: %d\n", sum);
    /* printf("unused space: %d\n", unused_space); */
    /* printf("space needed: %d\n", space_needed); */
    /* printf("root size: %d\n", root_size); */
    printf("Part 2 answer: %d\n", vector_peek_at(&to_deleted, 0));
    return 0;
}

int get_type(const char c)
{
    if (c == '$')
        return INPUT_CMD;
    if (isdigit(c))
        return INPUT_FILE;
    return INPUT_DIR;
}
