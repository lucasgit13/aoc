#include <stdio.h>
#include <stdlib.h>
#include "handlers.h"

#define SIZE 128

int fill_str(char * group[], FILE * stream);

int main(int argc, char *argv[]) {
    if (argc < 2) exit(1);
    FILE * fd;
    fd = fopen(argv[1], "r");
    if (fd == NULL) {
        fprintf(stderr, "cannot open file: %s", argv[1]);
        exit(1);
    }

    char str1[SIZE];
    char str2[SIZE];
    char str3[SIZE];

    char *group[3] = {
        str1,
        str2,
        str3
    };

    Deck deck = { 0, { 0 } };
    int ret, value = 0;
    int sum = 0;

    while (1) {
        ret = fill_str(group, fd);
        if(ret == -1) break;

        for (int i = 0; str1[i] != '\0'; i++) {
            for (int j = 0; j < str2[j] != '\0'; j++) {
                for (int k = 0; k < str3[k] != '\0'; k++) {
                    if (str1[i] == str2[j] && str1[i] == str3[k]) {
                        add_if_not_in(&deck, str1[i]);
                    }
                }
            }
        }

        for (int k = 0; k < deck.size; k++) {
            value = get_priority(deck.types[0]);
            /* printf("type: %c\n", deck.types[k]); */
            /* printf("value: %d\n", value); */
            sum += value;
        }
        deck.size = 0;
    }

    printf("sum: %d\n", sum);

    return 0;
}

int fill_str(char *group[], FILE *stream) {
    int ret;
    for (int i = 0; i < 3; i++) {
        ret = fscanf(stream, "%s", group[i]);
    }
    return ret;
}
