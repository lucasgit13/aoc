#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "handlers.h"


int main(int argc, char *argv[])
{
    if(argc<2) exit(1);
    FILE* fd;
    fd = fopen(argv[1], "r");
    if(fd==NULL) {
        fprintf(stderr, "cannot open file: %s", argv[1]);
        exit(1);
    }

    Deck deck = {0, {0}};
    char str[MAX];

    int size;
    int sum=0;
    int value;

    while(fscanf(fd, "%s", str)!=EOF){
        size = strlen(str)/2;

        for (int i = 0; i < size; i++) {
            for (int j = size; j < size*2; j++) {
                if(str[i]==str[j]) {
                    add_if_not_in(&deck, str[i]);
                    continue;
                }
            }
        }

        /* sum+=get_priority(deck.types[0]); */
        /* assert(deck.size==1); */
        for (int k = 0; k < deck.size; k++) {
            value = get_priority(deck.types[k]);
            sum+=value;
        }

        /* printf("%d\n", value); */
        /* printf("%s\n", str); */
        /* printf("types: %s\n", deck.types); */
        /* printf("priority: %d\n", value); */
        /* printf("size: %d\n\n", deck.size); */
        deck.size = 0;
    }

    printf("sum: %d\n", sum);

    return 0;
}
