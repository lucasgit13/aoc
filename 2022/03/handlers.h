#define MAX 128

typedef struct {
    int size;
    char types[MAX];
} Deck;

int add_if_not_in(Deck *deck, char c) {
    int size = deck->size;
    for (int i = 0; i < size; i++) {
        if(deck->types[i]==c) return 1;
    }
    deck->types[size] = c;
    deck->size++;
    return 0;
}

int get_priority(char type) {
    if((int)type <= 90 && (int)type >= 65) {
        return ((int)type - 65) + 27;
    }
    if((int)type>=97 && (int)type<=122) {
        return (int)type - 96;
    }
    return 1000;
}
