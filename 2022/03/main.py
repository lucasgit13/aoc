import sys
def rucksacks():
    with open(sys.argv[1], "r") as file:
        for line in file:
            yield line.strip()


def get_priority(type_c: str) -> int:
    char = ord(type_c)
    if char <= 90 and char >= 65:
        return int((char - 65) + 27)
    if char >= 97 and char <= 122:
        return int(char - 96)
    raise


sum: int = 0

for rucksack in rucksacks():
    half = int(len(rucksack) / 2)

    c1 = rucksack[:half]
    c2 = rucksack[half:]
    types = set()

    for i, _ in enumerate(c1):
        for j, _ in enumerate(c2):
            if c1[i] == c2[j]:
                types.add(c1[i])

    for t in types:
        sum += int(get_priority(t))
        # print(int(get_priority(t)))

print(f"sum: {sum}")
