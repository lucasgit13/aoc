#include <stdio.h>
#include <stdlib.h>

#define SIZE 128

typedef struct {
    char letter;
    int reps;
} Inst;


int fill_str(char *group[], FILE *stream) {
    int ret;
    for (int i = 0; i < 3; i++) {
        ret = fscanf(stream, "%s", group[i]);
    }
    return ret;
}

int main(int argc, char *argv[])
{
    FILE* fd = fopen("./input2.txt", "r");
    char str1[SIZE];
    char str2[SIZE];
    char str3[SIZE];
    int ret, count = 0;

    char *group[] = {str1, str2, str3};

    while(1){
        ret = fill_str(group, fd);
        if(ret==-1) break;

        puts(str1);
        puts(str2);
        puts(str3);
        /* count++; */
    }
    /* printf("count: %d\n", count-1); */

    return 0;
}


