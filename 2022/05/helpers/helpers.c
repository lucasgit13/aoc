#include "helpers.h"
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int count_stacks(const char* buff, int size)
{
    int count = 0;
    for (int i = 0; i < size; i++) {
        if (buff[i] == '[' || buff[i] == ']') {
            count++;
        }
    }
    if (count)
        return count / 2;
    return count;
}

int fgets_c(FILE* stream)
{
    char c = fgetc(stream);
    if (c == '\n')
        return 0;
    if (isalpha(c)) {
        return c;
    }
    return 1;
}

void vector_print(struct Vector* vector)
{
    long size = vector->size;
    putc('[', stdout);
    for (int i = 0; i < size - 1; i++) {
        printf("%c, ", vector->data[i]);
    }
    printf("%c]\n", vector->data[size - 1]);
}

void vector_print_last(struct Vector* vector)
{
    long size = vector->size;
    putc(vector->data[size - 1], stdout);
}

void vector_free(struct Vector* vector)
{
    free(vector->data);
    free(vector);
}

struct Vector* vector_new()
{
    struct Vector* vec = malloc(sizeof(struct Vector));
    *vec = vector_create();
    return vec;
}

struct Vector vector_create()
{
    char* ptr = calloc(VEC_MAX, sizeof(char));
    struct Vector vec = {ptr, VEC_MAX, 0};
    return vec;
}

void vector_reverse(struct Vector* vector)
{
    if ((vector->size) > 1) {
        int size = vector->size;
        int last = size - 1;
        int until = size / 2;
        char temp;
        for (int i = 0; i < until; i++) {
            temp = vector->data[i];
            vector->data[i] = vector->data[last - i];
            vector->data[last - i] = temp;
        }
    }
}

void vector_resize(struct Vector* vector)
{
    char* ptr = realloc(vector->data, (vector->max + VEC_MAX) * sizeof(char));
    if (ptr) {
        vector->data = ptr;
        vector->max += VEC_MAX;
    }
    else {
        fprintf(stderr, "out of memory..");
        exit(1);
    }
}

void vector_add_char(struct Vector* vector, char crate)
{
    if (vector->size == vector->max)
        vector_resize(vector);
    vector->data[vector->size] = crate;
    vector->size++;
    vector->data[vector->size] = '\0';
}

void vector_reset(struct Vector* vector)
{
    vector->size = 0;
    vector->data[0] = '\0';
}

void int_add(struct Int* int_, int num)
{
    int_->nums[int_->count] = num;
    int_->count++;
}

void long_add(struct Long* arr, long num)
{
    arr->data[arr->count] = num;
    arr->count++;
}

void long_reset(struct Long* arr) { arr->count = 0; }

void move_crate_v2(int amount, struct Vector* from, struct Vector* to,
                   struct Vector* temp)
{
    if (amount == 1) {
        move_crate(amount, from, to);
        temp->size = 0;
        return;
    }
    move_crate(amount, from, temp);
    move_crate(amount, temp, to);
}

void move_crate(int amount, struct Vector* from, struct Vector* to)
{
    for (int i = 0; i < amount; i++) {
        vector_add_char(to, from->data[(from->size) - 1]);
        from->size--;
    }
}

void extract_number_from(const char* str, struct Vector* vector,
                         struct Long* arr)
{
    int length = strlen(str) + 1;
    int prev = 0;
    for (int i = 0; i < length; i++) {
        if (isdigit(str[i])) {
            vector_add_char(vector, str[i]);
            prev = 1;
        }
        else if (prev) {
            long_add(arr, strtol(vector->data, NULL, 10));
            vector_reset(vector);
            prev = 0;
        }
    }
}

void long_print(struct Long* arr)
{
    long size = arr->count;
    putc('[', stdout);
    for (int i = 0; i < size - 1; i++) {
        printf("%ld, ", arr->data[i]);
    }
    printf("%ld]\n", arr->data[size - 1]);
}

int start_point(FILE* stream)
{
    char c = 1;
    int count = 0;
    while (c) {
        c = fgetc(stream);
        if (c == 'm') {
            break;
        }
        count++;
    }
    return count;
}

int get_stack(struct Int* stacks_idx, int i)
{
    for (int j = 0; stacks_idx->count; j++) {
        if (stacks_idx->nums[j] == i) {
            return j;
        }
    }
    return -1;
}

void fill(struct Vector* vec_stack[], struct Int* stacks_idx, const char* buff)
{
    int idx;
    for (int i = 0; buff[i] != '\0'; i++) {
        if (isalpha(buff[i])) {
            idx = get_stack(stacks_idx, i);
            vector_add_char(vec_stack[idx], buff[i]);
        }
    }
}

int get_letter_index(const char* str, char* c, int* idx)
{
    for (int i = 0; str[i] != '\0'; i++) {
        if (isalpha(str[i])) {
            *c = str[i];
            *idx = i;
        }
    }
    return -1;
}
