#ifndef HELPERS_H
#define HELPERS_H
#include <stdio.h>

#define VEC_MAX 32
#define VEC_LST_SIZE 16

struct Vector {
    char* data;
    int max;
    int size;
};

struct Int {
    int count;
    int nums[24];
};

struct Long {
    int count;
    long data[32];
};

struct Inst {
    long amount;
    struct Vector* from;
    struct Vector* to;
};

void vector_reverse(struct Vector* vector);
void vector_add_char(struct Vector* vector, char crate);
void vector_reset(struct Vector* vector);
void vector_resize(struct Vector* vector);
void vector_free(struct Vector* vector);
void vector_print(struct Vector* vector);
void vector_print_last(struct Vector* vector);

void int_add(struct Int* int_, int num);

int start_point(FILE* stream);

void long_add(struct Long* arr, long num);
void long_reset(struct Long* arr);
void long_print(struct Long* arr);

void extract_number_from(const char* str, struct Vector* vector,
                         struct Long* arr);
void move_crate(int amount, struct Vector* from, struct Vector* to);
void move_crate_v2(int amount, struct Vector* from, struct Vector* to,
                   struct Vector* temp);

int get_letter_index(const char* str, char* c, int* idx);
void fill(struct Vector* vec_stack[], struct Int* stacks_idx, const char* buff);
int count_stacks(const char* buff, int size);
int fgets_c(FILE* stream);

struct Vector vector_create();
struct Vector* vector_new();

#endif
