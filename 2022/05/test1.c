#include "helpers/helpers.h"

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define SIZE 64

typedef struct Long Long;
typedef struct Vector Vector;

int main()
{
    FILE* fd = fopen("./input.txt", "r");
    int proc = start_point(fd);
    char buff[SIZE];

    Long arr = {0, {0}};
    Vector *string = vector_new();

    fseek(fd, proc, SEEK_SET);
    while (fgets(buff, SIZE, fd)) {
        extract_number_from(buff, string, &arr);
        long_print(&arr);
        long_reset(&arr);
    }

    return 0;
}

