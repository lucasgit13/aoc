#include "helpers/helpers.h"

#include <assert.h>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define SIZE 64
typedef struct Vector Vector;
typedef struct Long Long;
typedef struct Int Int;
typedef struct Inst Inst;

int main(int argc, char** argv)
{
    if (argc < 2) {
        fprintf(stderr, "you need to provide an input file");
        exit(EXIT_FAILURE);
    }
    FILE* fd = fopen(argv[1], "r");
    if (!fd) {
        fprintf(stderr, "cannot open file: %s", argv[1]);
        exit(EXIT_FAILURE);
    }
    /* FILE* fd = fopen("./example.txt", "r"); */

    char buff[SIZE];
    Int stacks_idx = {0};
    Vector* vecs[32];

    int len = 0, count = 0, max = 0;
    while (fgets(buff, SIZE, fd)) {
        count = count_stacks(buff, SIZE);
        if (count > max)
            max = count;
        if (count == 0)
            break;
        len += strlen(buff);
    }

    for (int i = 0; i < max; i++) {
        vecs[i] = vector_new();
    }

    fseek(fd, len, SEEK_SET);
    fgets(buff, SIZE, fd);

    for (long i = 0; buff[i] != '\0'; i++) {
        if (isdigit(buff[i])) {
            int_add(&stacks_idx, i);
        }
    }

    fseek(fd, 0, SEEK_SET);
    for (int i = 0; i < max; i++) {
        fgets(buff, SIZE, fd);
        fill(vecs, &stacks_idx, buff);
    }

    // reversing vectors elements
    for (int i = 0; i < max; i++) {
        vector_reverse(vecs[i]);
    }

    /**
     * Rearrangement procedure
     */
    Long arr = {0, {0}};
    Vector* string = vector_new();
    Vector* temp_stack = vector_new();
    Inst operation = {0, NULL, NULL};

    fseek(fd, 0, SEEK_SET);
    int proc = start_point(fd);

    fseek(fd, proc, SEEK_SET);
    while (fgets(buff, SIZE, fd)) {
        extract_number_from(buff, string, &arr);
        /* long_print(&arr); */
        operation.amount = arr.data[0];
        operation.from = vecs[arr.data[1] - 1];
        operation.to = vecs[arr.data[2] - 1];

        // Part 1
        /* move_crate(operation.amount, operation.from, operation.to); */

        // Part 2
        move_crate_v2(operation.amount, operation.from, operation.to, temp_stack);

        long_reset(&arr);
    }

    for (int i = 0; i < max; i++) {
        /* vector_print(vecs[i]); */
        vector_print_last(vecs[i]);
    }

    free(string);
    free(temp_stack);
    fclose(fd);
    for (int i = 0; i < max; i++) {
        vector_free(vecs[i]);
    }
    return 0;
}

