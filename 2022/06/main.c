#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int strlen_c(const char* str);
int check_maker(char* str, int size);

int main(int argc, char** argv)
{
  int SIZE = 5;
  FILE* fd = fopen("./input.txt", "r");
  if (argc==3) {
    fd = fopen(argv[1], "r");
    if (atoi(argv[2])==2) SIZE = 15;
  }
  if (!fd) {
    fprintf(stderr, "cannot open file: %s", argv[1]);
    exit(1);
  }
  char buff[SIZE];
  int count = 0, len = 0, result = 0;

  while (fgets(buff, SIZE, fd)) {
    len = strlen_c(buff);
    if (len < (SIZE-1))
      break;
    result = check_maker(buff, SIZE);
    if (result)
      break;
    count++;
    fseek(fd, count, SEEK_SET);
  }

  printf("result: %d\n", count+(SIZE-1));

  return 0;
}

int strlen_c(const char* str)
{
  int count = strlen(str);
  if (str[count - 1] == '\n')
    return count - 1;
  return count;
}

int check_maker(char* str, int size)
{
  for (int i = 0; i <  size; i++) {
    for (int j = i + 1; j < size; j++) {
      if (str[i] == str[j])
	return 0;
    }
  }
  return 1;
}
