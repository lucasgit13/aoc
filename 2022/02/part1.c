#include <stdio.h>
#include <stdlib.h>

#include "helpers.h"

#define SIZE 16

int main(int argc, char *argv[])
{
    FILE *fd;
    if(argc<2) {
        fprintf(stderr, "you must provide a input file\n");
        exit(1);
    } else {
        fd = fopen(argv[1], "r");
        if(fd==NULL){
            fprintf(stderr, "cannot open file: %s\n", argv[1]);
            exit(1);
        }
    }

    Player player = {1,0,NONE};
    Player oponent = {2,0,NONE};

    char buff[SIZE];
    while(str_fcanf(fd, buff, SIZE)!=1){
        oponent.hand = to_hand(buff[0]);
        player.hand = to_hand(buff[2]);
        player.score += round_play(player.hand, oponent.hand);
    }

    printf("score: %d\n", player.score);
    return 0;
}

