import sys
class Hand:
    NONE = 0
    ROCK = 1
    PAPER = 2
    SCISSORS = 3

class rpts:
    LOST = 0
    DRAW = 3
    WON = 6

class Player:
    def __init__(self, id, score, hand) -> None:
        self.id = id
        self.score = score
        self.hand = hand

def get_round():
    with open(sys.argv[1], "r") as file:
        for line in file:
            yield line.split()

    return 1

def to_hand(c: str):
    match c:
        case 'A': return Hand.ROCK;
        case 'B': return Hand.PAPER;
        case 'C': return Hand.SCISSORS;

        case 'X': return Hand.ROCK;
        case 'Y': return Hand.PAPER;
        case 'Z': return Hand.SCISSORS;

        case _: return Hand.NONE;

def round_result(you, op) -> int:
    if you==op: return rpts.DRAW+player.hand
    if you==Hand.PAPER and op==Hand.ROCK: return rpts.WON+Hand.PAPER
    if you==Hand.ROCK and op==Hand.SCISSORS: return rpts.WON+Hand.ROCK
    if you==Hand.SCISSORS and op==Hand.PAPER: return rpts.WON+Hand.SCISSORS
    return player.hand

if __name__ == '__main__':
    player = Player(1, 0, Hand.NONE)
    oponent = Player(2, 0, Hand.NONE)


    for round in get_round():
        op, you = round
        oponent.hand = to_hand(op)
        player.hand = to_hand(you)
        player.score += round_result(player.hand, oponent.hand)

    print(f"score: {player.score}")
