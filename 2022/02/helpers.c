#include <stdio.h>

#include "helpers.h"

Hand to_hand(char c)
{
    switch (c) {
        case 'A': return ROCK;
        case 'B': return PAPER;
        case 'C': return SCISSORS;

        case 'X': return ROCK;
        case 'Y': return PAPER;
        case 'Z': return SCISSORS;

        default: return NONE;
    }
}

Round_pts end_with(char c)
{
    switch (c) {
        case 'X': return LOST;
        case 'Y': return DRAW;
        case 'Z': return WON;
        default: return -1;
    }
}

Hand hand_lose(Hand hand)
{
    switch (hand) {
        case ROCK:
            return SCISSORS;
        case PAPER:
            return ROCK;
        case SCISSORS:
            return PAPER;
        default:
            return hand;
    }
}

Hand hand_win(Hand hand)
{
    switch (hand) {
        case ROCK:
            return PAPER;
        case PAPER:
            return SCISSORS;
        case SCISSORS:
            return ROCK;
        default:
            return hand;
    }
}

int round_play(Hand you, Hand op)
{
    if(you==op) return DRAW+you;
    if(you==PAPER&&op==ROCK) return WON+PAPER;
    if(you==ROCK&&op==SCISSORS) return WON+ROCK;
    if(you==SCISSORS&&op==PAPER) return WON+SCISSORS;
    return you;
}

int round_play_v2(Hand op, Round_pts guide)
{
    switch (guide) {
        case DRAW:
            return round_play(op, op);
        case LOST:
            return round_play(hand_lose(op), op);
        case WON:
            return round_play(hand_win(op), op);
    }
    return 0;
}

int str_fcanf(FILE* stream, char* str, int size)
{
    char c;
    for (int i = 0; i < size; i++) {
        c = fgetc(stream);
        if(c==EOF) return 1;
        if(c=='\n') {
            str[i]='\0';
            return 0;
        }
        str[i]=c;
    }
    return 0;;
}

