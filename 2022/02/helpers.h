#ifndef HELPERS_H
#define HELPERS_H

typedef enum Hand {
    NONE,
    ROCK = 1,
    PAPER,
    SCISSORS
} Hand;

typedef enum Round_pts {
    LOST = 0,
    DRAW = 3,
    WON = 6
} Round_pts;

typedef struct Player {
    int id;
    int score;
    Hand hand;
} Player;

Round_pts end_with(char c);
Hand to_hand(char c);
Hand hand_lose(Hand hand);
Hand hand_win(Hand hand);
int round_play(Hand you, Hand op);
int round_play_v2(Hand op, Round_pts guide);
int str_fcanf(FILE* stream, char* str, int size);

#endif
