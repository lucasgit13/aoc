#include <stdio.h>
#include <stdlib.h>

#include "helpers.h"

#define MAX 16

int main()
{
    Vector *vec = malloc(sizeof(Vector));
    *vec = vector_create();

    for (long i = 0; i < 50; i++) {
        vector_add(vec, i*1024);
    }

    for (int i = 0; i < vec->size; i++) {
       printf("%ld, ", vec->nums[i]);
    }
    puts("\n");

    free(vec);
    return 0;
}

