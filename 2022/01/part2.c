#include <stdlib.h>

#include "helpers.h"

#define SIZE 128

long* resize_nums(long* ptr, long size);

int main(int argc, char *argv[]) {
    FILE *fd;
    if(argc<2) {
        fprintf(stderr, "you must provide a input file\n");
        exit(1);
    } else {
        fd = fopen(argv[1], "r");
        if(fd==NULL){
            fprintf(stderr, "cannot open file: %s\n", argv[1]);
            exit(1);
        }
    }

    char buff[SIZE] = {0};
    int ret = 0;
    long sum = 0;
    long initial = 32;

    long *nums = malloc(sizeof(long) * initial);
    Vector vec = {initial, 0, nums};

    while(ret!=1){
        ret=c_fgets(buff, SIZE, fd);
        if(ret==0){
            sum+=strtol(buff, NULL, 10);
        }
        if(ret==1||ret==3){
            /* printf("%ld\n", sum); */
            vector_add_v2(&vec, sum);
            sum=0;
        }
        if(vec.size == vec.max) {
            long new_size = (vec.max + initial) * 8;
            nums = resize_nums(nums, new_size);
            vec.nums = nums;
            vec.max+=initial;
        }
    }

    vector_sort(&vec);
    /* int size = vec->size; */
    /* printf("last: %ld\n", vec->nums[size-1]); */

    for (int i = 0; i < vec.size; i++) {
        printf("%ld, ", vec.nums[i]);
    }
    puts("\n");

    free(nums);
    return 0;
}

long* resize_nums(long* ptr, long size)
{
    ptr = realloc(ptr, size);
    if(ptr==NULL) {
        fprintf(stderr, "out of memmory\n");
        exit(1);
    }
    return ptr;
}
