#include <stdlib.h>

#include "helpers.h"

#define ADDITIONAL 128

Vector vector_create() {
    Vector vec = {32, 0, {0}};
    return vec;
}

Vector* vector_resize(Vector* vec) {
    vec = realloc(vec, ((vec->max + ADDITIONAL) * 8) + 8);
    vec->max+=ADDITIONAL;
    return vec;
}

Vector* vector_add(Vector* vec, long num){
    if(vec->max==vec->size) vec = vector_resize(vec);
    vec->nums[vec->size] = num;
    vec->size++;
    return vec;
}

void vector_add_v2(Vector* vec, long num){
    vec->nums[vec->size] = num;
    vec->size++;
}

void vector_swap(Vector* self, int from_idx, int to_idx) {
    long old = self->nums[from_idx];
    long new = self->nums[to_idx];
    self->nums[from_idx] = new;
    self->nums[to_idx] = old;
}

int get_lowerst_idx(Vector* vec, int start_from){
    long num = vec->nums[start_from];
    int idx = 0;
    for (int i = start_from; i < vec->size; i++) {
        for (int j = start_from; j < vec->size; j++) {
            if (vec->nums[i]<=vec->nums[j]) {
                if(vec->nums[i]<=num) {
                    num=vec->nums[i];
                    idx=i;
                }
            }
        }
    }
    return idx;
}

void vector_sort(Vector* vec) {
    int count=0;
    int idx;
    for (int i = 0; i < vec->size; i++) {
        idx=get_lowerst_idx(vec, count);
        if(idx==count) {
            count++;
            continue;
        }
        vector_swap(vec, count, idx);
        count++;
    }
}

int c_fgets(char* str, int size, FILE* stream){
    char c, p;
    c=fgetc(stream);
    if(c=='\n') return 3;
    if(c==EOF) return 1;
    for (int i = 0; i < size; i++) {
        p=c;
        c=fgetc(stream);
        if(c==EOF) return 1;
        str[i]=p;
        if(c=='\n') {
            str[++i]='\0';
            return 0;
        }
    }
    return 0;
}
