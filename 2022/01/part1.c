#include <stdlib.h>

#include "helpers.h"

#define SIZE 16

int main(int argc, char *argv[]) {
    FILE *fd;
    if(argc<2) {
        fprintf(stderr, "you must provide a input file\n");
        exit(1);
    } else {
        fd = fopen(argv[1], "r");
        if(fd==NULL){
            fprintf(stderr, "cannot open file: %s\n", argv[1]);
            exit(1);
        }
    }

    char buff[SIZE] = {0};
    int ret = 0;
    long sum, bigger = 0;

    while(ret!=1){
        ret=c_fgets(buff, SIZE, fd);
        if(ret==0){
            sum+=strtol(buff, NULL, 10);
        }
        if(ret==1||ret==3){
            /* printf("%ld\n", sum); */
            if(sum>bigger) bigger=sum;
            sum=0;
        }
    }
    printf("bigger: %ld\n", bigger);
    return 0;
}
