#include <stdio.h>
#include <stdlib.h>

typedef struct {
    int* nums;
} Vector;

int* n_realloc(int* ptr, int len){
    ptr = realloc(ptr, len);
    if(ptr == NULL){
        fprintf(stderr, "error\n");
        exit(1);
    }
    return ptr;
}

int main(int argc, char *argv[])
{
    int s = 26;
    int n = 36;
    int* p = calloc(s, sizeof(int));
    Vector vec = {p};

    p = n_realloc(vec.nums, n*4);

    for (int i = 0; i < n; i++) {
        *(p+i) = i;
    }

    for (int i = 0; i < n; i++) {
       printf("%d, ", p[i]);
    }
    puts("\n");

    free(p);
    return 0;
}

