import sys
file = sys.argv[1]
def work():
    global file
    with open(file, "rb") as file:
        for line in file:
            yield line

    yield b"\n"


def main():
    # eof = work()
    sum = 0
    value = 0
    num = 0
    for i in work():
        num = i.decode("utf-8")
        if i == b"\n":
            if sum > value:
                value = sum
            sum = 0
        else:
            sum += int(num)

    return value


if __name__ == "__main__":
    v = main()
    print(v)
