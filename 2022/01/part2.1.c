#include <stdlib.h>

#include "helpers.h"

#define SIZE 128

int main(int argc, char *argv[]) {
    FILE *fd;
    if(argc<2) {
        fprintf(stderr, "you must provide a input file\n");
        exit(1);
    } else {
        fd = fopen(argv[1], "r");
        if(fd==NULL){
            fprintf(stderr, "cannot open file: %s\n", argv[1]);
            exit(1);
        }
    }

    char buff[SIZE] = {0};
    int ret = 0;
    long sum = 0;

    Vector *vec = malloc(sizeof(Vector));
    *vec = vector_create();

    while(ret!=1){
        ret=c_fgets(buff, SIZE, fd);
        if(ret==0){
            sum+=strtol(buff, NULL, 10);
        }
        if(ret==1||ret==3){
            /* printf("%ld\n", sum); */
            vec = vector_add(vec, sum);
            sum=0;
        }
    }

    vector_sort(vec);
    /* int size = vec->size; */
    /* printf("last: %ld\n", vec->nums[size-1]); */

    for (int i = 0; i < vec->size; i++) {
        printf("%ld, ", vec->nums[i]);
    }
    puts("\n");

    free(vec);
    return 0;
}
