#ifndef HELPERS_H
#define HELPERS_H
#include <stdio.h>

typedef struct Vector {
    int max;
    int size;
    long nums[32];
} Vector;

int c_fgets(char* str, int size, FILE* stream);

Vector vector_create();

Vector* vector_resize(Vector* vec);

Vector* vector_add(Vector*, long num);

void vector_add_v2(Vector*, long num);

void vector_swap(Vector* self, int from_idx, int to_idx);

void vector_sort(Vector* vec);

int get_lowerst_idx(Vector* vec, int start_from);

#endif
