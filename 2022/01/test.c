#include <stdio.h>
#include <assert.h>


typedef struct {
    int size;
    int nums[32];
} Vector;

void vector_add(Vector* vec, int num){
    vec->nums[vec->size] = num;
    vec->size++;
}

void vector_swap(Vector* self, int from_idx, int to_idx) {
    int old = self->nums[from_idx];
    int new = self->nums[to_idx];
    self->nums[from_idx] = new;
    self->nums[to_idx] = old;
}

int get_lowerst(Vector* vec, int start_from){
    int num = vec->nums[start_from];
    for (int i = start_from; i < vec->size; i++) {
        for (int j = start_from; j < vec->size; j++) {
            if (vec->nums[i]<vec->nums[j]) {
                if(vec->nums[i]<=num) num=vec->nums[i];
            }
        }
    }
    return num;
}

int get_lowerst_idx(Vector* vec, int start_from){
    int num = vec->nums[start_from];
    int idx;
    for (int i = start_from; i < vec->size; i++) {
        for (int j = start_from; j < vec->size; j++) {
            if (vec->nums[i]<=vec->nums[j]) {
                if(vec->nums[i]<=num) {
                    num=vec->nums[i];
                    idx=i;
                }
            }
        }
    }
    return idx;
}

void vector_sort(Vector* vec) {
    int count=0;
    int idx;
    for (int i = 0; i < vec->size; i++) {
        idx=get_lowerst_idx(vec, count);
        if(idx==count) {
            count++;
            continue;
        }
        vector_swap(vec, count, idx);
        count++;
    }
}

int main(int argc, char *argv[])
{
    Vector vec = {0, {0}};
    vector_add(&vec, 12);
    vector_add(&vec, 2);
    vector_add(&vec, 72);
    vector_add(&vec, 23);
    vector_add(&vec, 99);
    vector_add(&vec, 42);
    vector_add(&vec, 99);
    vector_add(&vec, 1);

    for (int i = 0; i < vec.size; i++) {
       printf("%d, ", vec.nums[i]);
    }
    puts("\n");
    /* vector_swap(&vec, 0, 2); */

    /* for (int i = 0; i < vec.size; i++) { */
    /*    printf("%d, ", vec.nums[i]); */
    /* } */
    /* puts("\n"); */

    /* int l = get_lowerst_idx(&vec, 4); */
    /* printf("lowerst: %d\n", l); */

    vector_sort(&vec);

    for (int i = 0; i < vec.size; i++) {
       printf("%d, ", vec.nums[i]);
    }
    puts("\n");

    /* for (int i = 0; i < vec.size; i++) { */
    /*     for (int j = 0; j < vec.size; j++) { */
    /*         if(vec.nums[i]>vec.nums[j]) printf("vec.num[i] = %d\n", vec.nums[i]); */
    /*     } */
    /* } */

    return 0;

}
