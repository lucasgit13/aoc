#ifndef VECTOR_H_
#define VECTOR_H_

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>

#define VECTOR_INITIAL_SIZE 24

struct Vector {
    void* data;
    int count;
    int max;
    int size;
};

#define vector_peek_as_uint8_t(vec, at) \
    *((uint8_t*)vec.data + (sizeof(uint8_t) * at))

#define vector_push_as_uint8_t(vec, num) \
{   \
    if (vec.count==vec.max) vector_resize(&vec); \
        *(uint8_t*)(vec.data + (sizeof(uint8_t) * vec.count++)) = num;    \
}

#define vector_push_as(type, vec, num)		\
{ \
    if (vec->count==vec->max) vector_resize(vec); \
    *(typeof(type)*)(vec->data + (sizeof(typeof(type)) * vec->count++)) = num; \
}

#define vector_printf_as(type, format, vec)			\
{ \
    if(vec->count) { \
        putc('[', stdout);    \
        int count_t = vec->count -1; \
        for (int mi=0;mi<count_t; mi++) { \
            printf(format ", ", *(typeof(type)*)(vec->data + (sizeof(typeof(type)) * mi))); \
        } \
        printf(format "]\n", *(typeof(type)*)(vec->data + (sizeof(typeof(type)) * count_t))); \
    } else \
    puts("[]");                    \
}

#define vector_printf_as_str(type, vec)			\
{ \
    if(vec->count) { \
        putc('[', stdout);    \
        int count_t = vec->count -1; \
        for (int mi=0;mi<count_t; mi++) { \
            printf("%c", *(typeof(type)*)(vec->data + (sizeof(typeof(type)) * mi)) + '0'); \
        } \
        printf("%c]\n", *(typeof(type)*)(vec->data + (sizeof(typeof(type)) * count_t)) + '0'); \
    } else \
    puts("[]");                    \
}

#define vector_print_as_uint8_t(vec) \
{ \
    putc('[', stdout); \
    int count = vec.count -1; \
    for (int i=0;i<count; i++) { \
        printf("%u, ", *(uint8_t*)(vec.data + (sizeof(uint8_t) * i)));    \
    } \
    printf("%u]\n", *(uint8_t*)(vec.data + (sizeof(uint8_t) * count))); \
}

#define vector_peek_as(type, vec, idx) *((typeof(type)*)vec->data + idx)

#define vector_peek_printf_as(type, format, vec, idx) \
    printf(format, *((typeof(type)*)vec->data + idx))

#define vector_sort_as(type, vec)                                              \
    typeof(type) _temp;                                                        \
    int _count = vec->count;                                                   \
    for (int i = 0; i < _count; ++i) {                                         \
        for (int j = i + 1; j < _count; ++j) {                                 \
            if (*((typeof(type)*)vec->data + i) >=                             \
                *((typeof(type)*)vec->data + j)) {                             \
                _temp = *((typeof(type)*)vec->data + i);                       \
                *((typeof(type)*)vec->data + i) =                              \
                    *((typeof(type)*)vec->data + j);                           \
                *((typeof(type)*)vec->data + j) = _temp;                       \
            }                                                                  \
        }                                                                      \
    }

#define VEC_POOL_INC 24
#define vector_pool_push(pool, vec)    \
    if (pool.count==pool.max) vector_pool_resize(&pool); \
    pool.vecs[pool.count] = vec;    \
    pool.count++;   \

struct Vector_Pool {
    struct Vector** vecs;
    int count;
    int max;
    int size;
};

struct Vector_Pool vector_pool_create(int size, int pool_size);
void vector_pool_resize(struct Vector_Pool* pool);

struct Vector vector_create(int size);
void vector_resize(struct Vector* vec);
void* vector_peek_at(struct Vector* vec, int at);
struct Vector* vector_new(int size);

#endif

#ifdef VECTOR_IMPLEMENTATION

struct Vector vector_create(int size)
{
    void* ptr = calloc(VECTOR_INITIAL_SIZE, size);
    return(struct Vector){.data=ptr, .count=0, .max=VECTOR_INITIAL_SIZE, .size=size};
}

struct Vector* vector_new(int size)
{
    struct Vector* ptr = malloc(sizeof(struct Vector));
    *ptr = vector_create(size);
    return ptr;
}

void vector_resize(struct Vector* vec)
{
    void* ptr = realloc(vec->data, (vec->max + VECTOR_INITIAL_SIZE) * vec->size);
    vec->data = ptr;
    vec->max+=VECTOR_INITIAL_SIZE;
}

struct Vector_Pool vector_pool_create(int size, int pool_size)
{
    struct Vector** ptr = calloc(pool_size, sizeof(struct Vector**));
    return (struct Vector_Pool){.vecs=ptr, .count=0, .max=pool_size, .size=size};
}

void vector_pool_resize(struct Vector_Pool* pool)
{
    struct Vector** ptr = realloc(pool->vecs, (pool->max + VEC_POOL_INC) * sizeof(struct Vector**));
    pool->vecs = ptr;
    pool->max+=VEC_POOL_INC;
}

#endif
