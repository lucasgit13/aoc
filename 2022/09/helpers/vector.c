#include "vector.h"
#include <stdlib.h>


struct Vector vector_create(int size)
{
    void* ptr = calloc(VECTOR_INITIAL_SIZE, size);
    return(struct Vector){.data=ptr, .count=0, .max=VECTOR_INITIAL_SIZE, .size=size};
}

void vector_resize(struct Vector* vec)
{
    void* ptr = realloc(vec->data, (vec->max + VECTOR_INITIAL_SIZE) * vec->size);
    vec->data = ptr;
    vec->max+=VECTOR_INITIAL_SIZE;
}

void* vector_peek_at(struct Vector* vec, int at)
{
    return (vec->data + (at * vec->size));
}

struct Vector* vector_new(int size)
{
    struct Vector* ptr = malloc(sizeof(struct Vector));
    *ptr = vector_create(sizeof(size));
    return ptr;
}

struct Vector_Pool vector_pool_create(int size, int pool_size)
{
    struct Vector** ptr = calloc(pool_size, sizeof(struct Vector**));
    return (struct Vector_Pool){.vecs=ptr, .count=0, .max=pool_size, .size=size};
}

void vector_pool_resize(struct Vector_Pool* pool)
{
    struct Vector** ptr = realloc(pool->vecs, (pool->max + VEC_POOL_INC) * sizeof(struct Vector**));
    pool->vecs = ptr;
    pool->max+=VEC_POOL_INC;
}


