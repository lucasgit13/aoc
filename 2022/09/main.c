#define VECTOR_IMPLEMENTATION
#include "helpers/vector.h"
#include <assert.h>

#define N 1000
#define UPDATE_TAIL()                                                          \
    tail.row = head.prow;                                                      \
    tail.col = head.pcol


enum { UP, DOWN, RIGHT, LEFT } Direction;

struct cord {
    int x;
    int y;
};

struct Tail {
    int row;
    int col;
};

struct Head {
    int row;
    int col;

    int prow;
    int pcol;
};

int count = 1;
struct Head head = {.row = 0, .col = 0, .prow = 0, .pcol = 0};
struct Tail tail = {.row = 0, .col = 0};
struct Vector* vec;
struct Vector_Pool pool;

void move_head(int direction)
{
    head.prow = head.row;
    head.pcol = head.col;

    switch (direction) {
    case UP:
        head.row--;
        break;
    case DOWN:
        head.row++;
        break;
    case RIGHT:
        head.col++;
        break;
    case LEFT:
        head.col--;
        break;
    default:
        assert(0 && "Unreachable");
    }
}

void vector_push_cord(struct Vector* vec_cord, int x, int y)
{
    for (int i=0; i<vec_cord->count; i++) {
        if ((((struct cord*)((struct cord*)vec_cord->data + i))->x == x) && (((struct cord*)((struct cord*)vec_cord->data + i))->y == y)) {
            /* printf("%i\n", i); */
            return;
        }
    }
    *(struct cord*)((struct cord*)vec_cord->data + vec_cord->count++) = (struct cord){.x=x, .y=y};
    count++;
}

uint32_t hash(int a, int b)
{
    uint32_t r=(abs(a)*43265397)^(abs(b)*39123407);
    return r%N;
}

void move_tail(void)
{
    if ((tail.row == head.row) && (tail.col == head.col))
        return;
    if (abs(tail.row - head.row) > 1 || abs(tail.col - head.col) > 1) {
        UPDATE_TAIL();
        uint32_t idx = hash(tail.row, tail.col);
        vector_push_cord(pool.vecs[idx], tail.row, tail.col);
    }
}

void move(int direction, int n)
{
    for (int i = 0; i < n; i++) {
        move_head(direction);
        move_tail();
    }
}

int parse(char c)
{
    switch (c) {
    case 'U':
        return UP;
    case 'D':
        return DOWN;
    case 'R':
        return RIGHT;
    case 'L':
        return LEFT;
    default:
        assert(0 && "Unreachable");
    }
}

int vector_count_unique_as_int64_t(struct Vector* vec)
{
    int count=1;
    for (int i=0; i<(vec->count) - 1; i++) {
        if (*((int64_t*)vec->data + i)!=*((int64_t*)vec->data + i+1)) {
            count++;
        }
    }
    return count;
}

int main(int argc, char** argv)
{
    if (argc < 2) {
        fprintf(stderr, "you must provide an input file\n");
        exit(1);
    }
    FILE* fd = fopen(argv[1], "r");
    if (!fd) {
        fprintf(stderr, "cannot open file: %s\n", argv[1]);
        exit(1);
    }

    pool = vector_pool_create(sizeof(struct cord), N);
    vec = vector_new(sizeof(struct cord));
    char buf[16];

    for (int i=0; i<N; i++) {
        pool.vecs[i] = vector_new(sizeof(struct cord));
    }

    while (fgets(buf, 16, fd)) {
        /* printf("%s\n", buf); */
        move(parse(buf[0]), strtol(buf + 2, NULL, 10));
    }

    printf("part 1: %d\n", count);
    return 0;
}
