x = set()
hashes = []

class Direction:
    UP=0
    DOWN=1
    RIGHT=2
    LEFT=3

class Head:
    row = 4
    col = 0
    prow = 4
    pcol = 0

class Tail:
    row = 4
    col = 0


def move_head(direction: int):
    Head.prow = Head.row
    Head.pcol = Head.col

    match direction:
        case 0:
            Head.row-=1
        case 1:
            Head.row+=1
        case 2:
            Head.col+=1
        case 3:
            Head.col-=1
        case _:
            assert False, "Unreachable"

def hash_p(a, b):
    r=(a*43265397)^(b*39123407)
    if a<0: r^=0b1
    return r

def move_tail():
    global count
    if Head.col == Tail.col and Head.row == Tail.row:
        return
    if abs(Head.col - Tail.col) > 1 or abs(Head.row - Tail.row) > 1:
        Tail.row = Head.prow;
        Tail.col = Head.pcol;
        x.add((Tail.row, Tail.col))
        # h=hash_p(Tail.row, Tail.col)
        # print(f"{h%150} : {h}")
        return

def parse_direction(char: str) -> int:
    match char:
        case 'U':
            return 0
        case 'D':
            return 1
        case 'R':
            return 2
        case 'L':
            return 3
        case _:
            assert False, "Unreachable"

def move(direction: int, amount: int):
    for _ in range(amount):
        move_head(direction)
        move_tail()

from sys import argv, stderr
if len(argv) < 2:
    print("you need to provide an input file :/", file=stderr)
    exit(1)

file = open(argv[1], 'r')
lines = file.readlines()

for mov in lines:
    direction, amount = mov.strip('\n').split(' ')
    move(parse_direction(direction), int(amount))

print(f"part 1: {len(x)}")
# print(len(list(set(hashes))))
