#define VECTOR_IMPLEMENTATION
#include "helpers/vector.h"

struct cord {
    int x;
    int y;
};

#define push_cord(x, y) vector_push_cord(vec, x, y)

void vector_push_cord(struct Vector* vec, int x, int y)
{
    *(struct cord*)((struct cord*)vec->data + vec->count++) = (struct cord){.x=x, .y=y};
}

#define N 10
int64_t hash(int a, int b)
{
    int64_t r=(a*43265397)^(b*39123407);
    /* if (a<0) r^=0b1; */
    return r%N;
}

int main(void)
{
    struct Vector* vec= vector_new(sizeof(struct cord));
    struct Vector_Pool pool = vector_pool_create(sizeof(struct cord), N);

    pool.vecs[0] = vec;

    for (int i=0; i<N; i++) {
        pool.vecs[i] = vector_new(sizeof(struct cord));
    }

    push_cord(69, 20);
    push_cord(12, 43);

    vector_push_cord(pool.vecs[0], 69, 20);
    vector_push_cord(pool.vecs[0], 12, 43);

    /* printf("%d\n", ((struct cord*)((struct cord*)vec->data + 1))->y); */

    int a = 12;
    int b = 43;
    for (int i=0; i<vec->count; i++) {
        if ((((struct cord*)((struct cord*)pool.vecs[0]->data + i))->x == a) && (((struct cord*)((struct cord*)pool.vecs[0]->data + i))->y == b)) {
            puts("yes");
            printf("%i\n", i);
            break;
        }
    }

    return 0;
}
