#include "helpers/vector.h"
#include <ctype.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

static int size;
int top_check(struct Vector* map, int row, int col);
int bottom_check(struct Vector* map, int row, int col);
int right_check(struct Vector* map, int row, int col);
int left_check(struct Vector* map, int row, int col);

int top_view(struct Vector* map, int row, int col);
int bottom_view(struct Vector* map, int row, int col);
int right_view(struct Vector* map, int row, int col);
int left_view(struct Vector* map, int row, int col);

#define SIZE 5
#define vec_peek(map, row, col) *((char*)map->data + (size * row) + col)

#define vector_pp(vec, base)                                                   \
    puts("{");                                                                 \
    for (int i = 0; i < base; i++) {                                           \
        printf("  { %c, %c, %c, %c, %c },", *((char*)vec->data + (base * i)),  \
               *((char*)vec->data + (base * i) + 1),                           \
               *((char*)vec->data + (base * i) + 2),                           \
               *((char*)vec->data + (base * i) + 3),                           \
               *((char*)vec->data + (base * i) + 4));                          \
        puts("");                                                              \
    }                                                                          \
    puts("}");

static void pp(struct Vector* vec, int base)
{
    puts("{");
    for (int row = 0; row < base; ++row) {
        for (int col = 0; col < base; ++col) {
            printf("%2c,", *((char*)vec->data + (base * row) + col));
        }
        puts("");
    }
    puts("}");
}

int main(void)
{
    FILE* fd = fopen("./input.txt", "r");
    struct Vector* map = vector_new(sizeof(char));

    char c = fgetc(fd);
    int count = 0;
    while (c != EOF) {
        if (isdigit(c)) {
            vector_push_as(char, map, c);
            count++;
        }
        c = fgetc(fd);
    }

    size = sqrt(map->count);

    count = 4 * (size - 1);
    for (int r = 1; r < (size - 1); ++r) {
        for (int c = 1; c < (size - 1); ++c) {
            if (top_check(map, r, c)) {
                count++;
                continue;
            }
            if (right_check(map, r, c)) {
                count++;
                continue;
            }
            if (bottom_check(map, r, c)) {
                count++;
                continue;
            }
            if (left_check(map, r, c)) {
                count++;
                continue;
            }
        }
    }

    printf("part 1: %d\n", count);

    int prev = 0;
    count = 1;
    for (int r = 1; r < (size - 1); ++r) {
        for (int c = 1; c < (size - 1); ++c) {
            count *= top_view(map, r, c);
            count *= bottom_view(map, r, c);
            count *= right_view(map, r, c);
            count *= left_view(map, r, c);

            /* printf("count: %d\n", count); */
            if (count > prev)
                prev = count;
            count = 1;
        }
    }

    printf("part 2: %d\n", prev);

    return 0;
}

int top_view(struct Vector* map, int row, int col)
{
    char num = vec_peek(map, row, col);
    int count = 0;
    for (int i = row - 1; i != -1; --i) {
        count++;
        if (vec_peek(map, i, col) >= num)
            return count;
    }
    return count;
}

int bottom_view(struct Vector* map, int row, int col)
{
    char num = vec_peek(map, row, col);
    int count = 0;
    for (int i = row + 1; i < size; ++i) {
        count++;
        if (vec_peek(map, i, col) >= num)
            return count;
    }
    return count;
}

int right_view(struct Vector* map, int row, int col)
{
    char num = vec_peek(map, row, col);
    int count = 0;
    for (int i = col + 1; i < size; ++i) {
        count++;
        if (vec_peek(map, row, i) >= num)
            return count;
    }
    return count;
}

int left_view(struct Vector* map, int row, int col)
{
    char num = vec_peek(map, row, col);
    int count = 0;
    for (int i = col - 1; i != -1; --i) {
        count++;
        if (vec_peek(map, row, i) >= num)
            return count;
    }
    return count;
}

int top_check(struct Vector* map, int row, int col)
{
    char num = vec_peek(map, row, col);
    for (int i = 0; i < row; ++i) {
        if (vec_peek(map, i, col) >= num) {
            return 0;
        }
    }
    return 1;
}

int bottom_check(struct Vector* map, int row, int col)
{
    char num = vec_peek(map, row, col);
    for (int i = row + 1; i < size; ++i) {
        if (vec_peek(map, i, col) >= num) {
            return 0;
        }
    }
    return 1;
}

int right_check(struct Vector* map, int row, int col)
{
    char num = vec_peek(map, row, col);
    for (int i = col + 1; i < size; ++i) {
        if (vec_peek(map, row, i) >= num) {
            return 0;
        }
    }
    return 1;
}

int left_check(struct Vector* map, int row, int col)
{
    char num = vec_peek(map, row, col);
    for (int i = 0; i < col; ++i) {
        if (vec_peek(map, row, i) >= num) {
            return 0;
        }
    }
    return 1;
}
