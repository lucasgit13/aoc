#include "helpers/buffer.h"
#include "helpers/helpers.h"
#include "helpers/vector.h"
#include <ctype.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define buffer_foreach(buf, type, iter, value)                                 \
    for (type iter = value; iter < buf.count; ++iter)

int main(void)
{
    FILE* fd = fopen("./input.txt", "r");
    /* FILE* fd = fopen("./example.txt", "r"); */

    struct Buffer row_str = buffer_create();
    struct Vector_Pool map = vector_pool_create(sizeof(uint8_t), 32);
    struct Vector* row;

    uint8_t num = 0;
    while (fgets_buffer(&row_str, fd)) {
        row = vector_new(sizeof(uint8_t));
        vector_pool_push(map, row);
        /* printf("%s, %d\n", row_str.data, row_str.count); */
        buffer_foreach(row_str, int, i, 0)
        {
            if (isdigit(row_str.data[i])) {
                num = row_str.data[i] - '0';
                vector_push_as(uint8_t, map.vecs[map.count - 1], num);
            }
        }
        row_str.count = 0;
    }

    int cols_count, count = 0;
    cols_count = map.count;
    /* rows_count = (*map.vecs)->count; */

    count = 4 * (cols_count - 1);
    for (int r = 1; r < (cols_count - 1); ++r) {
        for (int c = 1; c < (cols_count - 1); ++c) {
            if (top_check(&map, r, c)) {
                count++;
                continue;
            }
            if (right_check(&map, r, c)) {
                count++;
                continue;
            }
            if (bottom_check(&map, r, c)) {
                count++;
                continue;
            }
            if (left_check(&map, r, c)) {
                count++;
                continue;
            }
        }
    }
    printf("part 1: %d\n", count);

    struct Vector* views = vector_new(sizeof(int));

    count = 1;
    for (int r = 1; r < (cols_count - 1); ++r) {
        for (int c = 1; c < (cols_count - 1); ++c) {
            count *= top_view(&map, r, c);
            count *= bottom_view(&map, r, c);
            count *= right_view(&map, r, c);
            count *= left_view(&map, r, c);

            /* printf("count: %d\n", count); */
            if (count > 1)
                vector_push_as(int, views, count);
            count = 1;
        }
    }

    /* printf("top: %d\n", top_view(&map, 1, 1)); */

    vector_sort_as(int, views);

    /* vector_printf_as(int, "%u", views); */
    vector_peek_printf_as(int, "part 2: %u\n", views, views->count - 1);

    return 0;
}
