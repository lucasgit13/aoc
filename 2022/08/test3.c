#include "helpers/helpers.h"
#include "helpers/vector.h"
#include "helpers/buffer.h"
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>

int main(void)
{
    FILE* fd = fopen("./example.txt", "r");
    struct Buffer buf = buffer_create();
    struct Vector* vec = {0};
    struct Vector_Pool pool = vector_pool_create(sizeof(uint8_t), 32);

    while (fgets_buffer(&buf, fd)) {
	vec = vector_new(sizeof(uint8_t));
	for (int i=0; i<buf.count; i++) {
	    if (isdigit(buf.data[i])) {
		vector_push_as(uint8_t, vec, buf.data[i] - '0');
	    }
	}
	
	vector_pool_push(pool, vec);
	
	buf.count = 0;
    }

    int r, c, n, result = 1;
    r = 1;
    c = 2;
    n = *((uint8_t*)pool.vecs[r]->data + c);

    /* *((uint8_t*)vec->data + 3) = *((uint8_t*)vec->data + 1); */
    vector_sort_as(uint8_t, vec);
    vector_printf_as(uint8_t, "%u", vec);
    printf("some: %u\n", vector_peek_as(uint8_t, vec, 3));

    printf("num: %u\n", n);
    printf("top view: %4d\n", result=top_view(&pool, r, c));
    printf("right view: %2d\n", result=right_view(&pool, r, c));
    printf("bottom view: %d\n", result=bottom_view(&pool, r, c));
    printf("left view: %3d\n", result=left_view(&pool, r, c));
    
    return 0;

    printf("pool count: %d\n", pool.count);

    /* vector_printf_as_str(int, pool.vecs[33]); */

    for (int i=0; i<pool.count; i++) {
	printf("i: %d", i);
	vector_printf_as_str(int, pool.vecs[i]);
    }
    

    return 0;
}



