#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define SIZE 99
#define MAP_SIZE 100
#define SV_LEN 150

struct Map {
    int data[100][100];
    int count;
};

struct Map map = {0};

struct String_View {
    char data[SV_LEN];
    int count;
};

int strlen_nl(char* str)
{
    int len = strlen(str);
    if (str[len - 1] == '\n') {
        str[len - 1] = 0x00;
        return len - 1;
    }
    return len;
}

void map_print(int count)
{
    for (int i = 0; i < count; ++i) {
        for (int j = 0; j < count; ++j) {
            printf("%d", map.data[i][j]);
        }
        puts("");
    }
}

int top_check(int row, int col)
{
    int num = map.data[row][col];
    for (int i = 0; i < row; ++i) {
        if (map.data[i][col] >= num) {
            return 0;
        }
    }
    return 1;
}

int bottom_check(int row, int col)
{
    int num = map.data[row][col];
    for (int i = row + 1; i < map.count; ++i) {
        if (map.data[i][col] >= num) {
            return 0;
        }
    }
    return 1;
}

int right_check(int row, int col)
{
    int num = map.data[row][col];
    for (int i = col + 1; i < map.count; ++i) {
        if (map.data[row][i] >= num) {
            return 0;
        }
    }
    return 1;
}

int left_check(int row, int col)
{
    int num = map.data[row][col];
    for (int i = 0; i < col; ++i) {
        if (map.data[row][i] >= num) {
            return 0;
        }
    }
    return 1;
}

int main(void)
{
    FILE* fd = fopen("./input.txt", "r");

    struct String_View sv = {0};
    int max_cols = 0;

    fgets(sv.data, SV_LEN, fd);
    max_cols = strlen_nl(sv.data);

    fseek(fd, 0, SEEK_SET);

    int map_count = 0;
    while (fgets(sv.data, SV_LEN, fd)) {
        for (int i = 0; i < max_cols; ++i) {
            map.data[map_count][i] = sv.data[i] - '0';
        }
        map_count++;
    }
    map.count = max_cols;

    printf("count: %d\n", map_count);
    /* map_print(map_count); */

    int count = 0;
    count = 4 * (max_cols - 1);
    for (int i = 1; i < (max_cols - 1); ++i) {
        for (int j = 1; j < (max_cols - 1); ++j) {
            if (top_check(i, j)) {
                count++;
                continue;
            }
            if (right_check(i, j)) {
                count++;
                continue;
            }
            if (bottom_check(i, j)) {
                count++;
                continue;
            }
            if (left_check(i, j)) {
                count++;
                continue;
            }
        }
    }

    printf("count: %d\n", count);

    /* int r=3; */
    /* int c=2; */

    /* printf("num: %d\n", map.data[r][c]); */
    /* printf("top: \t\t%d\n", top_check(r, c)); */
    /* printf("left: \t\t%d\n", left_check(r, c)); */
    /* printf("bottom: \t%d\n", bottom_check(r, c)); */
    /* printf("right: \t\t%d\n", right_check(r, c)); */

    return 0;
}
