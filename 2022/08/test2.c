#include "helpers/vector.h"
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

#define SIZE 5

int main(void)
{
    struct Vector_Pool pool = vector_pool_create(sizeof(uint8_t), 12);
    struct Vector* vec0 = vector_new(sizeof(uint8_t));
    struct Vector* vec1 = vector_new(sizeof(uint8_t));
    vector_pool_push(pool, vec0);
    pool.vecs[1] = vec1;

    vector_push_as(uint8_t, pool.vecs[0], 10);
    vector_push_as(uint8_t, pool.vecs[0], 34);
    vector_printf_as(uint8_t, "%u", pool.vecs[0]);


    return 0;
}

