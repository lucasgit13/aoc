
#ifndef HELPERS_H
#define HELPERS_H
#include "vector.h"

#define get_map(map, idx0, idx1) *(uint8_t*)((*(map.vecs + idx0))->data + idx1)

int bottom_check(struct Vector_Pool* map, int row, int col);
int top_check(struct Vector_Pool* map, int row, int col);
int right_check(struct Vector_Pool* map, int row, int col);
int left_check(struct Vector_Pool* map, int row, int col);

int top_view(struct Vector_Pool* map, int row, int col);
int bottom_view(struct Vector_Pool* map, int row, int col);
int right_view(struct Vector_Pool* map, int row, int col);
int left_view(struct Vector_Pool* map, int row, int col);

#endif
