#include "helpers.h"
#include "buffer.h"
#include "vector.h"

static uint8_t get_from_map(struct Vector_Pool* pool, int r, int c)
{
    return *(uint8_t*)(pool->vecs[r]->data + c);
}

int top_check(struct Vector_Pool* map, int row, int col)
{
    uint8_t num = get_from_map(map, row, col);
    for (int i = 0; i < row; ++i) {
        if (get_from_map(map, i, col) >= num) {
            return 0;
        }
    }
    return 1;
}

int bottom_check(struct Vector_Pool* map, int row, int col)
{
    uint8_t num = get_from_map(map, row, col);
    for (int i = row + 1; i < map->count; ++i) {
        if (get_from_map(map, i, col) >= num) {
            return 0;
        }
    }
    return 1;
}

int right_check(struct Vector_Pool* map, int row, int col)
{
    uint8_t num = get_from_map(map, row, col);
    for (int i = col + 1; i < map->count; ++i) {
        if (get_from_map(map, row, i) >= num) {
            return 0;
        }
    }
    return 1;
}

int left_check(struct Vector_Pool* map, int row, int col)
{
    uint8_t num = get_from_map(map, row, col);
    for (int i = 0; i < col; ++i) {
        if (get_from_map(map, row, i) >= num) {
            return 0;
        }
    }
    return 1;
}

/* View Check */

int top_view(struct Vector_Pool* map, int row, int col)
{
    uint8_t num = get_from_map(map, row, col);
    int count = 0;
    for (int i = row - 1; i != -1; --i) {
        /* printf("%u\n", num); */
        /* printf("i: %d, %u\n", i, get_from_map(map, i, col)); */
        count++;
        if (get_from_map(map, i, col) >= num)
            return count;
    }
    return count;
}

int bottom_view(struct Vector_Pool* map, int row, int col)
{
    uint8_t num = get_from_map(map, row, col);
    int count = 0;
    for (int i = row + 1; i < map->count; ++i) {
        /* printf("%u\n", num); */
        /* printf("i: %d, %u\n", i, get_from_map(map, i, col)); */
        count++;
        if (get_from_map(map, i, col) >= num)
            return count;
    }
    return count;
}

int right_view(struct Vector_Pool* map, int row, int col)
{
    uint8_t num = get_from_map(map, row, col);
    int count = 0;
    for (int i = col + 1; i < map->count; ++i) {
        /* printf("%u\n", num); */
        /* printf("i: %d, %u\n", i, get_from_map(map, row, i)); */
        count++;
        if (get_from_map(map, row, i) >= num)
            return count;
    }
    return count;
}

int left_view(struct Vector_Pool* map, int row, int col)
{
    uint8_t num = get_from_map(map, row, col);
    int count = 0;
    for (int i = col - 1; i != -1; --i) {
        /* printf("%u\n", num); */
        /* printf("i: %d, %u\n", i, get_from_map(map, row, i)); */
        count++;
        if (get_from_map(map, row, i) >= num)
            return count;
    }
    return count;
}
