#ifndef BUFFER_H
#define BUFFER_H
#include <stdio.h>

#define BUF_SIZE 24

struct Buffer {
    char* data;
    int count;
    int max;
};

struct Buffer buffer_create(void);
void buffer_write(struct Buffer* str, char* src);
void buffer_write_char(struct Buffer* str, char c);
int fgets_buffer(struct Buffer* dest, FILE* stream);

#endif
