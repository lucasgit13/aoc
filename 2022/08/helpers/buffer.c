#include "buffer.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static void buffer_resize(struct Buffer* str, int new)
{
    char* ptr = realloc(str->data, (str->max + new) * sizeof(char));
    str->data = ptr;
    str->max += new;
}

struct Buffer buffer_create(void)
{
    char* ptr = calloc(BUF_SIZE, sizeof(char));
    return (struct Buffer){.data = ptr, .count = 0, .max = BUF_SIZE - 1};
}

void buffer_write(struct Buffer* str, char* src)
{
    int size = strlen(src);
    if (size > (str->max))
        buffer_resize(str, size + 1);
    memset(str->data, 0, size + 1);
    memcpy(str->data, src, size);
    str->count += size;
}

void buffer_write_char(struct Buffer* str, char c)
{
    if ((str->count + 1) >= str->max)
        buffer_resize(str, BUF_SIZE);
    str->data[str->count++] = c;
    str->data[str->count] = 0x00;
}

int fgets_buffer(struct Buffer* dest, FILE* stream)
{
    char c = fgetc(stream);
    if (c == EOF)
        return 0;
    while (c != '\n') {
        if (c == EOF)
            return 1;
        buffer_write_char(dest, c);
        c = fgetc(stream);
    }
    return 1;
}
