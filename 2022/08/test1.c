#include <stdio.h>
#include <stdint.h>

#define SIZE 5

int left_check(int row, int col, uint8_t map[5][5]);
int right_check(int row, int col, uint8_t map[5][5]);
int top_check(int row, int col, uint8_t map[5][5]);
int bottom_check(int row, int col, uint8_t map[5][5]);

int main(void)
{
    uint8_t map[SIZE][SIZE] = {
        { 3, 0, 3, 7, 3 },
        { 2, 5, 5, 1, 2 },
        { 6, 5, 3, 3, 2 },
        { 3, 3, 5, 4, 9 },
        { 3, 5, 3, 9, 0 },
    };

    /* int row, col, num, count = 0, r = 0, row_lim, col_lim; */

/*     row = 3; */
/*     col = 2; */
/*     num = map[row][col]; */

    int result = 0, count = 0;
    for (int r=1; r<(SIZE-1); ++r) {
        for (int c=1; c<(SIZE-1); ++c) {
            /* printf("num: %u\n", map[r][c]); */
            if(top_check(r, c, map)) {
                count++;
                continue;
            }
            /* result = top_check(r, c, map); */
            /* printf("top: \t\t%d\n", result); */

            if(left_check(r, c, map)){
                count++;
                continue;
            }
            /* result = left_check(r, c, map); */
            /* printf("left: \t\t%d\n", result); */

            if(bottom_check(r, c, map)){
                count++;
                continue;
            }
            /* result = bottom_check(r, c, map); */
            /* printf("bottom: \t%d\n", result); */

            if(right_check(r, c, map)) {
                count++;
                continue;
            }
            /* result = right_check(r, c, map); */
            /* printf("right: \t\t%d\n", result); */
            /* puts("\n"); */
        }
        /* puts("\n"); */
    }

    /* r = top_check(row, col, map); */
    /* printf("top: \t\t%d\n", r); */

    /* r = left_check(row, col, map); */
    /* printf("left: \t\t%d\n", r); */

    /* r = bottom_check(row, col, map); */
    /* printf("bottom: \t%d\n", r); */

    /* r = right_check(row, col, map); */
    /* printf("right: \t\t%d\n", r); */

    int outer = SIZE*2 + (SIZE-2)*2;
    printf("count: %d\n", count+outer);

    return 0;

}

int bottom_check(int row, int col, uint8_t map[5][5])
{
    uint8_t num = map[row][col];
    for (int i=row+1; i<SIZE; ++i) {
        if (map[i][col]>=num) {
            return 0;
        }
    }
    return 1;
}

int top_check(int row, int col, uint8_t map[5][5])
{
    uint8_t num = map[row][col];
    for (int i=0; i<row; ++i) {
        if (map[i][col]>=num) {
            return 0;
        }
    }
    return 1;
}

int right_check(int row, int col, uint8_t map[5][5])
{
    uint8_t num = map[row][col];
    for (int i=col+1; i<SIZE; ++i) {
        if (map[row][i]>=num) {
            return 0;
        }
    }
    return 1;
}

int left_check(int row, int col, uint8_t map[5][5])
{
    uint8_t num = map[row][col];
    for (int i=0; i<col; ++i) {
        if (map[row][i]>=num) {
            return 0;
        }
    }
    return 1;
}
