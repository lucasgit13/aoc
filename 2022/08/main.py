file="./input.txt"

lines = open(file, "r").readlines()

map = []

for line in lines:
    map.append(line.rstrip('\n'))

total = len(map)
last_idx = total - 1

def top_check(row, col):
    num = map[row][col]
    for i in range(row):
        if map[i][col] >= num:
            return 0;
    return 1;

def bottom_check(row, col):
    num = map[row][col]
    for i in range(row+1, total):
        if map[i][col] >= num:
            return 0;
    return 1;

def right_check(row, col):
    num = map[row][col]
    for i in range(col+1, total):
        if map[row][i] >= num:
            return 0;
    return 1;

def left_check(row, col):
    num = map[row][col]
    for i in range(col):
        if map[row][i] >= num:
            return 0;
    return 1;

# View
def top_view(row, col):
    num = map[row][col]
    count = 0
    for i in range(row-1, -1, -1):
        count+=1
        if map[i][col]>=num: return count
    return count

def bottom_view(row, col):
    num = map[row][col]
    count = 0
    for i in range(row+1, total):
        count+=1
        if map[i][col]>=num: return count
    return count

def right_view(row, col):
    num = map[row][col]
    count = 0
    for i in range(col+1, total):
        count+=1
        if map[row][i]>=num: return count
    return count

def left_view(row, col):
    num = map[row][col]
    count = 0
    for i in range(col-1, -1, -1):
        count+=1
        if map[row][i]>=num: return count
    return count


# r = 2
# c = 3
# print(f"num: {map[r][c]}")
# print(f"top: \t\t{top_check(r, c)}")
# print(f"right: \t\t{right_check(r, c)}")
# print(f"bottom: \t{bottom_check(r, c)}")
# print(f"left: \t\t{left_check(r, c)}")

# print(f"top: \t\t{top_view(r, c)}")
# print(f"right: \t\t{right_view(r, c)}")
# print(f"bottom: \t{bottom_view(r, c)}")
# print(f"left: \t\t{left_view(r, c)}")

# exit(0)

count = 0
count = (total * 2) + (total - 2) * 2
for r in range(1, last_idx):
    for c in range(1, last_idx):
        if top_check(r, c):
            count+=1
            continue
        if right_check(r, c):
            count+=1
            continue
        if bottom_check(r, c):
            count+=1
            continue
        if left_check(r, c):
            count+=1
            continue

print(f"part 1: {count}")

views = []
count = 1
for r in range(1, last_idx):
    for c in range(1, last_idx):
        count*=top_view(r, c)
        count*=bottom_view(r, c)
        count*=right_view(r, c)
        count*=left_view(r, c)
        if count>1: views.append(count)
        count = 1

views.sort()

print(f"part 2: {views[-1]}")
# print(f"[{map[7]}]")

# for line in map:
#     print(f"{line}, {len(line)}")

