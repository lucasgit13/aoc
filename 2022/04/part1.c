#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

#define SIZE 16

void get_number(const char* str, int size, char* out);

int main(int argc, char **argv) {
    FILE *fd;
    if(argc<2) {
        fprintf(stderr, "you must provide a input file\n");
        exit(1);
    } else {
        fd = fopen(argv[1], "r");
        if(fd==NULL){
            fprintf(stderr, "cannot open file: %s\n", argv[1]);
            exit(1);
        }
    }

    long pair[4] = {0};
    char buff[SIZE], num[SIZE];
    fgets(buff, SIZE, fd);

    for (int i = 0; i < 4; i++) {
        get_number(buff, SIZE, num);
        pair[i] = strtol(num, NULL, 10);
    }

    for (int i = 0; i < 4; i++) {
        printf("pair[i] = %ld\n", pair[i]);
    }

    return 0;
}

int fgets_c(FILE* stream)

void get_number(const char* str, int size, char* out)
{
    int count = 0;
    for (int i = 0; i < size; i++) {
        if(isdigit(str[i]))
        {
            out[count] = str[i];
            count++;
        }
        else
        {
            out[count+1] = '\0';
            break;
        }
    }
}
