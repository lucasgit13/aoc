#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#define SIZE		16
#define GROUP_SIZE	4
#define VEC_MAX		4

#define	F1	vec.nums[0]
#define	F2	vec.nums[1]
#define	S1	vec.nums[2]
#define	S2	vec.nums[3]

#define OP str_op[2]

typedef struct Vector {
	int max;
	int size;
	long nums[GROUP_SIZE];
} Vector;

typedef struct String_t {
	int len;
	char array[16];
} String_t;

int (*part_operation)(int a, int b);
int part1(int a, int b);
int part2(int a, int b);
void print_vector(Vector* vec);
int isInRange(long a, long b, long check);
void split_longs(Vector* vec, const String_t* s);
void String_t_print(String_t* str);
int fgets_c(FILE* stream, String_t* str);


int main(int argc, char **argv) {
	//errno_t err;
	FILE* fd;
	char str_op[24];
	int go = 0;

	//err = fopen_s(&fd, "input.txt", "r");
	if (argc != 3) {
		fprintf(stderr, "you must provide a flag for which part and an input file.\n\t%s -p[1 or 2] <input file>\n", argv[0]);
		exit(EXIT_FAILURE);
	}
	fd = fopen(argv[2], "r");
	strcpy(str_op, argv[2]);

	if (strcmp(argv[1], "-p1") == 0)
	{
		part_operation = part1;
		go = 1;
	}

	if (strcmp(argv[1], "-p2") == 0) 
	{
		part_operation = part2;
		go = 1;
	}

	if (go != 1) {
		fprintf(stderr, "unkown part: %s\n", argv[1]);
		exit(EXIT_FAILURE);
	}

	if (!fd) {
		fprintf(stderr, "cannot open file: %s\n", argv[1]);
		exit(EXIT_FAILURE);
	}

	String_t str = { 0 };
	Vector vec = { VEC_MAX, 0, {0} };
	int count = 0;

	while (fgets_c(fd, &str)) {
		split_longs(&vec, &str);
		//print_vector(&vec);
		if (part_operation(isInRange(F1, F2, S1), isInRange(F1, F2, S2))) {
			//String_t_print(&str);
			count++;
		} 
		else if (part_operation(isInRange(S1, S2, F1), isInRange(S1, S2, F2))) {
			//String_t_print(&str);
			count++;
		}
		vec.size = 0;
		str.len = 0;
	}

	printf("count: %d\n", count);

	return 0;
}

int part1(int a, int b)
{
	if (a && b) return 1;
	return 0;
}

int part2(int a, int b)
{
	if (a || b) return 1;
	return 0;
}

void print_vector(Vector* vec)
{
	for (int i = 0; i < vec->size; i++)
	{
		printf("%ld, ", vec->nums[i]);
	}
	puts("\n");
}

int isInRange(long a, long b, long check)
{
	if (a <= check && check <= b) {
		return 1;
	}
	return 0;
}

void split_longs(Vector* vec, const String_t* s)
{
	char temp[16] = { 0 };
	int count = 0;
	for (int i = 0; i < s->len + 1; i++) {
		if (isdigit(s->array[i])) {
			temp[count] = s->array[i];
			count++;
		}
		else {
			temp[count] = '\0';
			vec->nums[vec->size] = strtol(temp, NULL, 10);
			vec->size++;
			count = 0;
		}
	}
}

void String_t_print(String_t* str)
{
	printf("%s\n", str->array);
}

int fgets_c(FILE* stream, String_t* str)
{
	char c = fgetc(stream);
	if (c == EOF) return 0;
	if (c == '\n') return 1;

	while (1) {
		if (c == EOF) {
			str->array[str->len] = '\0';
			return 1;
		}
		if (c == '\n') {
			str->array[str->len] = '\0';
			return 1;
		}
		str->array[str->len] = c;
		str->len += 1;
		c = fgetc(stream);
	}
	return 1;
}
