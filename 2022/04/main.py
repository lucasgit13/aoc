import sys
file = sys.argv[1]

def isInRange(a: int, b: int, check: int) -> int:
    if a <= check <= b:
        return 1
    return 0

def get_pairs(file):
    with open(file, "r") as file:
        for line in file:
            yield line.strip()

count = 0
for pair in get_pairs(file):
    group = pair.split(',')
    first = group[0].split('-')
    second = group[1].split('-')

    f1, f2 = list(map(int, first))
    s1, s2 = list(map(int, second))

    if isInRange(f1, f2, s1) and isInRange(f1, f2, s2):
        # print(pair)
        count+=1
    elif isInRange(s1, s2, f1) and isInRange(s1, s2, f2):
        # print(pair)
        count+=1

print(count)
# .234.....  2-4
# .....678.  6-8

# .23......  2-3
# ...45....  4-5

# ....567..  5-7
# ......789  7-9

# .2345678.  2-8
# ..34567..  3-7

# .....6...  6-6
# ...456...  4-6

# .23456...  2-6
# ...45678.  4-8
